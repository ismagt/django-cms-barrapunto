from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from .models import Gestor, Page
from xml.sax.handler import ContentHandler
from xml.sax import make_parser

import sys
import urllib.request

# Create your views here.
page= """
<head>
  <title>CMS-Barrapunto</title>
</head>
<font color="red" size=6>Bienvenido al portal de noticias</font><br> <br>
<font color="red" size=4>Hemos filtrado las noticias con sus titulares, con un simple CLICK 
accederá a su contenido</font> <br> <br>	
"""

page_template_database = """
>>> <a href="{enlace}"> {titles} <br><br>
"""

page_template = """
> <a href="{enlace}"> {titles} <br><br>
"""

page_content = """
<p><a href="/"> Volver a la pagina de inicio</a></p> <br> <br>
<head>
  <title>{titulo}</title>
</head>
<font color="blue" size=6>Contenido fundamental de la noticia</font><br> <br>
<font color="black" size=4>{contenido}</font> <br> <br>
<font color="red" size=6>Enlaces a otras noticias de interes</font><br> <br>	
"""

page_others = """

<size=4><a href="/{titles}"> {titles}</a><br> <br>	
"""

page_error = """
<head>
  <title>Error de recurso</title>
</head>
<font color="red" size=7>ERROR!</font><br> <br>
<font color="blue" size=5>El enlace proporcionado no pertenece a ningun recurso</font> <br> <br>	
"""

formulario1 = """
<form action = "" method ="POST">
<br>
	Escriba el nombre del recurso, recuerda que para dirigirse al contenido /<name>:<br>
	<input type="text" name="nombre"><br><br>
	Escriba el contenido del recurso<br>
	<input type="text" name="contenido"><br><br>
	<input type="submit" value="Enviar">
	</form>
	<br>
<font color="black" size=4>Lo que he añadido nuevo es:</font> <br> <br>	
{new} <br>	
"""

class myContentHandler(ContentHandler):

    def __init__ (self):
        self.inItem = False
        self.inContent = False
        self.theContent = ""
        # Al principio de todo es necesario restaurar la BBDD
        d = Gestor.objects.all()
        d.delete()
        d1 = Page.objects.all()
        d1.delete()

    def startElement (self, name, attrs):
        # Los datos de las páginas se encuentras tras la etiqueta 'item'.
        # Nota apararece otros 'link' y 'title' que no me interesa.
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            # Esto en el contenido buscado cuando leo la etiqueta 'title' o 'link'.
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.inContent = True
            elif name == 'description':
                self.inContent = True

    def endElement (self, name):
        # Salgo de '/item', luego ya no estoy en el contenido buscado.
        if name == 'item':
            self.inItem = False
        elif self.inItem:
            if name == 'title':
                self.line = self.theContent
                # To avoid Unicode trouble
                self.inContent = False
                self.theContent = ""
            elif name == 'link':
                self.link = "<a href='" + self.theContent + "'>" + self.line + "</a><br>"
                self.inContent = False
                self.theContent = ""
            elif name == 'description':
                self.des = self.theContent + "<br>"
                self.inContent = False
                self.theContent = ""

                # Realizo el guardado del contenido RSS en la BBDD.
                # Dicho contenido se actualizara cuando volvamos a la pagina principal.
                # Lo realicé así para mostrar al usuario un menu de los recursos = title
                new = Gestor(title = self.line, url = self.link, content = self.des)
                print(new.title)
                new.save()

    def characters (self, chars):
        # Si estoy en el contenido, añado a la varible inContent los caracteres.
        if self.inContent:
            self.theContent = self.theContent + chars


def home(request):
    try:
        theParser = make_parser()
        theHandler = myContentHandler()
        theParser.setContentHandler(theHandler)

        xmlFile = urllib.request.urlopen('http://barrapunto.com/index.rss')
        theParser.parse(xmlFile)

        elements = Gestor.objects.all()
        urls=""
        titles=""   
        total = ""
        
        for element in elements:
            titles = element.title
            urls = element.url
            enlace = request.build_absolute_uri() +  element.title
            total += page_template_database.format(titles=titles, enlace=enlace)
            response1 = page + total
            
        #~ contenidos = Page.objects.all()
        #~ nombres = ""
        #~ total1 = ""            
        #~ for contenido in contenidos:
            #~ nombres=contenido.nombre
            #~ link = request.build_absolute_uri() +  contenido.nombre
            #~ total1 += page_template.format(titles=nombres, enlace=link)
            #~ total1 += total1 + "<br>"           
        if request.method == "GET":                               
            return HttpResponse(formulario1.format(new="") + response1)
        elif request.method == "POST":
            new = Page(nombre = request.POST['nombre'], contenido = request.POST['contenido'])
            new.save()        
            
            contenidos = Page.objects.all()
            nombres = ""
            total1 = ""            
            for contenido in contenidos:
                nombres=contenido.nombre
                link = request.build_absolute_uri() +  contenido.nombre
                total1 += page_template.format(titles=nombres, enlace=link)
                
            response2 = formulario1.format(new=total1)			
            return HttpResponse(response2 + response1)       
    except Gestor.DoesNotExist:
        return HttpResponseNotFound(page_error.format())

def content(request, key): 
    try:    
        resource1 = Page.objects.get(nombre=key)
        contenidos = Page.objects.all()
        nombres=""
        link=""
        total1=""
        for contenido in contenidos:
            nombres=contenido.nombre
            link = request.build_absolute_uri() +  contenido.nombre
            total1 += page_template_database.format(titles=nombres, enlace=link)
      
            contenido = page_content.format(contenido=resource1.contenido, titulo = resource1.nombre)
            response = contenido + total1
            return HttpResponse(response)
    except Page.DoesNotExist:
            return HttpResponseNotFound(page_error.format())
       
             
#~ GET ----> mostrar un formulario y sino error (404)
#~ POST ----> actualizar el contenido         
